import styled from "styled-components";

export const PaginationStyled = styled.div`
  margin-top: 15px;
  padding-left: 20px;
  padding-right: 20px;
`

export const PaginationItemStyled = styled.div<{isActive: boolean}>`
  cursor: pointer;
  margin-left: 20px;
  float: left;
  border: 1px solid black;
  width: 24px;
  height: 24px;
  display: flex;
  align-items: center;
  justify-content: center;
  &:first-child{
    margin-left: 0;
  }
  ${props => props.isActive ? 'background-color: grey;' : ''}
`