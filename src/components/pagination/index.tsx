import {FC} from "react";
import {PaginationItemStyled, PaginationStyled} from "./styled";

export interface IPagination {
  page: number,
  countPage: number,
  onChangePage: (newPage: number) => void
}

export const Pagination:FC<IPagination> = (props) => {
  const pagesList = []
  for(let i = 0; i < props.countPage ; i++){
    pagesList.push(i)
  }

  const onChangePage = (newPage: number) => {
    if(newPage !== props.page){
      props.onChangePage(newPage)
    }
  }

  return (
    <PaginationStyled>
      {pagesList.map((value, index) =>(
        <PaginationItemStyled key={index} isActive={index + 1 === props.page} onClick={() => onChangePage(index + 1)}>{index + 1}</PaginationItemStyled>
      ))}
    </PaginationStyled>
  )
}