import {AppThunk} from "../store";
import {setPosts} from "./slices";
import {requestController} from "../../utils/requestController/requestController";

export const getPostsData = (page: number = 1, search: string = ''): AppThunk => async (
  dispatch: any
) => {
  let url = `/posts?limit=10&skip=${page * 10 - 10}`
  if(search){
    url = `/posts/search?limit=10&skip=${page * 10 - 10}&q=${search}`
  }
  dispatch(requestController({
    id: `getPostsData`,
    url: url,
    method: 'GET',
    action: setPosts,
  }))
};