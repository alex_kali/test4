import {RootState, store} from "../store";
import {getIsError, getIsLoading, useDataSelector, useIsError, useIsLoading} from "react-redux-request-controller";
import {getPostsData} from "./thunks";
import { useSelector } from 'react-redux'
import {setSearch} from "./slices";

export const Posts = {
  useData: () => useDataSelector(
    (state: RootState) => state.posts.posts?.posts,
    Posts.object.getIsLoading,
    getPostsData
  ) as Array<IPost> | null,

  useIsLoading: () => useIsLoading('getPostsData'),
  useIsError: () => useIsError('getPostsData'),

  pagination: {
    useCountPages: () => useSelector((state: RootState) => Math.ceil(state.posts.posts?.total / 10)) as number, // state.posts.posts?.limit)) as number, // некоректно работает бэк
    useNumberPage: () => useSelector((state: RootState) => Math.ceil(+state.posts.posts?.skip / 10 + 1)) as number,
  },

  object: {
    getData: () => store.getState().posts.posts as Array<IPost> | null,
    getIsLoading: () => getIsLoading('getPostsData'),
    getIsError: () => getIsError('getPostsData'),
    getSearch: () =>  store.getState().posts.search as string
  },

  options: {
    changePage: (newPage: number) => window.dispatch(getPostsData(newPage, Posts.object.getSearch())),
    changeSearch: (search: string) => {
      window.dispatch(getPostsData(1, search))
      window.dispatch(setSearch(search))
    },
  }
}



export interface IPost {
  id: string | number;
  title: string;
  body: string;
  userId: string | number;
  tags: Array<string>;
  reactions: string | number;
}