import {createSlice} from "@reduxjs/toolkit";

export const initialState: any = {
  posts: {
    posts: null,
    limit: 10,
    skip: '0',
    total: 0,
  },
  search: ''
};

export const postsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {
    setPosts: (state, action) => {
      state.posts = action.payload
    },
    setSearch: (state, action) => {
      state.search = action.payload
    },
  },
});

export const { setPosts, setSearch } = postsSlice.actions

export default postsSlice.reducer;