import {configureStore, ThunkAction, Action, combineReducers} from '@reduxjs/toolkit';
import { formControllerReducer } from 'react-redux-hook-form';
import {requestControllerReducer} from "react-redux-request-controller";
import postsReducer from './posts/slices';
import postReducer from './post/slices';

window.getState = () => store.getState()

const combinedReducer = combineReducers({
  formController: formControllerReducer, // подключение formControllerReducer
  requestController: requestControllerReducer, // подключение requestControllerReducer

  posts: postsReducer,
  post: postReducer,
});

const rootReducer = (state:any, action:any) => {
  if (action.type === 'user/logoutUser') {
    for(let i in state){
      if(i !== 'formController'){
        state[i] = undefined
      }
    }
  }
  return combinedReducer(state, action);
};

export const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
  >;