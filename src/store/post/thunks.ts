import {AppThunk} from "../store";
import {requestController} from "../../utils/requestController/requestController";
import {setPost} from "./slices";

export const getPostData = (id: number | string): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: `getPostData`,
    url: `/posts/${id}`,
    method: 'GET',
    action: setPost,
  }))
};