import {useDataSelector, useIsError, useIsLoading} from "react-redux-request-controller";
import {RootState} from "../store";
import {IPost} from "../posts/model";
import {getPostData} from "./thunks";
import {setPost} from "./slices";

export const Post = {
  useData: (id: number) => useDataSelector(
    (state: RootState) => state.post.post,
    () => false,
    () => getPostData(id),
  ) as IPost | null,

  useIsLoading: () => useIsLoading('getPostData'),
  useIsError: () => useIsError('getPostData'),

  options: {
    setPost : (post: IPost) => window.dispatch(setPost(post))
  }
}