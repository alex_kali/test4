import {FC} from "react";
import {PostsStyled} from "./styled";
import {Posts} from "../../store/posts/model";
import {PostView} from "./post";
import {Pagination} from "../../components/pagination";
import {Search} from "./search";

export const PostsPage:FC = () => {
  const postsData = Posts.useData()
  const postsIsLoading = Posts.useIsLoading()
  const postsPaginationCount = Posts.pagination.useCountPages()
  const postsPaginationPage = Posts.pagination.useNumberPage()

  return (
    <PostsStyled>
      <Search/>
      {postsData === null || postsIsLoading ? <>loading...</> : postsData.map((post) => <PostView post={post} key={post.id}/>)}
      <Pagination countPage={postsPaginationCount} page={postsPaginationPage} onChangePage={Posts.options.changePage} />
    </PostsStyled>
  )
}