import styled from "styled-components";

export const PostsStyled = styled.div`
  
`

export const PostViewStyled = styled.div`
  
`

export const PostViewTitleStyled = styled.div`
  cursor: pointer;
`

export const SearchStyled = styled.div`
  max-width: 400px;
  margin: 20px;
`