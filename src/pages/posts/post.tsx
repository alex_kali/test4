import {FC} from "react";
import {IPost} from "../../store/posts/model";
import {PostViewStyled, PostViewTitleStyled} from "./styled";
import {Post} from "../../store/post/model";
import {useNavigate} from "react-router-dom";

export const PostView:FC<{post: IPost}> = (props) => {
  const navigate = useNavigate();
  const onClick = () => {
    Post.options.setPost(props.post)
    navigate(`/post/${props.post.id}`);
  }

  return (
    <PostViewStyled onClick={onClick}>
      <PostViewTitleStyled>{props.post.title}</PostViewTitleStyled>
    </PostViewStyled>
  )
}