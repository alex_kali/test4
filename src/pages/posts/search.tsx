import {FC} from "react";
import {useForm, WrapperForm} from "react-redux-hook-form";
import {TextInput} from "../../utils/formController/inputs/textInput";
import {SearchStyled} from "./styled";
import {SubmitButton} from "../../utils/formController/buttons";
import {Posts} from "../../store/posts/model";

export const Search:FC = () => {
  const form = useForm({name: 'search'})

  return (
    <SearchStyled>
      <WrapperForm form={form} onSubmit={(values: {search: string}) => Posts.options.changeSearch(values.search)}>
        <TextInput name={'search'}/>
        <SubmitButton text={'Search'}/>
      </WrapperForm>
    </SearchStyled>
  )
}