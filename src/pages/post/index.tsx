import {FC} from "react";
import {PostPageStyled} from "./styled";
import {useParams} from "react-router-dom";
import {Post} from "../../store/post/model";

export const PostPage:FC = () => {
  const { id } = useParams()
  const postData = Post.useData(id as any)
  const postIsLoading = Post.useIsLoading()

  if(!postData || postIsLoading){
    return <>Is loading!!!</>
  }

  return (
    <PostPageStyled>
      <div>{postData.title}</div>
      <div>{postData.body}</div>
    </PostPageStyled>
  )
}