import React, {memo} from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  useRoutes
} from "react-router-dom";
import {PostsPage} from "./pages/posts";
import {PostPage} from "./pages/post";

function App() {
  const AppRoutes = memo(() => {
    return useRoutes([
      { path: "/", element: <PostsPage /> },
      { path: "/post/:id", element: <PostPage /> },
    ]);
  });
  return (
    <Router>
      <AppRoutes />
    </Router>
  );
}

export default App;
