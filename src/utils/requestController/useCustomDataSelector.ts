import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";

export function useCustomDataSelector(selector: any, isLoading: () => boolean, getDefaultFunction: any) {
  const dispatch = useDispatch()
  const data: any = useSelector(selector)

  useEffect(() => {
    if(data === null && !isLoading()){
      dispatch(getDefaultFunction())
    }
  },[])

  return data
}
