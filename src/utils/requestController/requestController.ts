import {wrapperFetch} from "./wrapperFetch";
import {loadingFailed, loadingSuccess, startLoading} from "react-redux-request-controller/slice/requestControllerSlice";

interface IRequestController {
  onSuccess?: (data: any) => void;
  onFailed?: (errors: any | undefined) => void;
  serialize?: (data: any) => void;

  method: "POST" | "GET" | "DELETE" | "PUT";
  id: string;
  action: any;
  url: string;
  params?: object;
}

// @ts-ignore
export const requestController = (props:IRequestController): AppThunk => async (
  dispatch: any
) => {
  dispatch(startLoading(props.id))
  try {
    const response = await wrapperFetch(props)
    let data = await response.json();
    if(response.status === 400){
      dispatch(loadingFailed(props.id))
      if(props.onFailed){
        props.onFailed(data)
      }
    }else{
      dispatch(loadingSuccess(props.id))
      if(props.serialize){
        data = props.serialize(data)
      }
      if(props.onSuccess){
        props.onSuccess(data)
      }
      dispatch(props.action(data))
    }
  } catch {
    dispatch(loadingFailed(props.id))
    if(props.onFailed){
      props.onFailed(undefined)
    }
  }

}