interface IWrapperFetch {
  url: string;
  method: "POST" | "GET" | "DELETE" | "PUT";
  params?: object;
}

declare global {
  interface Window {
    token: string | undefined;
  }
}

export const wrapperFetch = async (props:IWrapperFetch) => {
  const params:any = {

  }
  if(props.params){
    params["body"] = JSON.stringify(props.params)
  }
  if(window.token){
    params["headers"]["Authorization"] = window.token
  }
  return await fetch(process.env.REACT_APP_SERVER_API_URL + props.url, params);
}