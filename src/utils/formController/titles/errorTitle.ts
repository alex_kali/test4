import styled from "styled-components";
import {ERROR_COLOR_INPUT, MARGIN_INPUT} from "../stylesConstant";


export const ErrorTitleStyled = styled.div`
  color: ${ERROR_COLOR_INPUT};
  margin-top: 4px;
  font-size: 16px;
  margin-bottom: ${MARGIN_INPUT};
  padding-left: 17px;
  padding-right: 17px;
`
