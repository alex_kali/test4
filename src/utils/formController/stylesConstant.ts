export const MARGIN_INPUT = '4px'
export const HEIGHT_INPUT = '40px'
export const BORDER_RADIUS_INPUT = '40px'
export const FONT_SIZE_INPUT = '18px'
export const PADDING_INPUT = '15px'
export const COLOR_INPUT = 'rgb(100,100,100)'
export const BORDER_INPUT = '2px solid rgb(100,100,100)'
export const WIDTH_INPUT = 'calc(100% - 34px)'

export const ERROR_COLOR_INPUT = 'rgba(255,99,99,1)'
export const ERROR_BORDER_COLOR_INPUT = 'rgba(255,99,99,1)'

export const OPACITY_INPUT = '0.3'
export const OPACITY_FOCUS_INPUT = '0.9'