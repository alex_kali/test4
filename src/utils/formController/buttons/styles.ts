import styled from "styled-components";
import {BORDER_INPUT, BORDER_RADIUS_INPUT, OPACITY_FOCUS_INPUT, OPACITY_INPUT} from "../stylesConstant";

interface ISubmitButtonStyled {
  isValidForm: boolean;
}

export const SubmitButtonStyled = styled.button<ISubmitButtonStyled>`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: auto;
  margin-right: auto;
  cursor: pointer;
  border: ${BORDER_INPUT};
  border-radius: ${BORDER_RADIUS_INPUT};
  margin-top: 15px;
  font-size: 18px;
  width: 100%;
  &:hover{
    background: rgb(0,0,0);
    border-color: rgb(0,0,0);
    color: white;
  }
  ${props => `
    opacity: ${props.isValidForm ? OPACITY_FOCUS_INPUT : OPACITY_INPUT};
    &:hover{
      cursor: ${props.isValidForm ? '' : 'not-allowed'};
    }
  `}
`
