﻿import React, {FC, memo} from 'react';
import {CustomInputStyled} from "./styled";
import {useField} from "react-redux-hook-form";
import {ErrorTitleStyled} from "../../titles/errorTitle";

interface IText {
  name: string;
  required?: boolean;
  className?: string;
  isTouch?: boolean;
}

export const TextInput: FC<IText> = memo((props) => {
  const {useData, useIsValidate, useIsTouch, useMessageError} = useField({
    name: props.name,
    isRequired: props.required,
    isTouch: props.isTouch,
    isValidate: props.isTouch,
  })
  const [data, changeData] = useData()
  const [isValidate, ] = useIsValidate()
  const [isTouch, ] = useIsTouch()
  const [messageError, ] = useMessageError()

  return (
    <>
      <CustomInputStyled
        value={data || ''}
        onChange={(e) => {changeData(e.target.value)}}
        isValidate={isValidate || !isTouch}
        className={props.className}
        id={props.name}
      />
      {!isValidate && isTouch &&
        <ErrorTitleStyled className={'error'}>
          {messageError}
        </ErrorTitleStyled>
      }
    </>
  )
});
